package com.llugo.simply;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class MyCenteringArrayAdapter extends ArrayAdapter {
    public MyCenteringArrayAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        return setCentered(super.getView(position, convertView, parent));
    }

    private View setCentered(View view)
    {
        TextView textView = (TextView)view.findViewById(android.R.id.text1);
        textView.setGravity(Gravity.CENTER);
        return view;
    }

    public int selectedItem = 0;

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        View v = null;
        v = super.getDropDownView(position, null, parent);
        // If this is the selected item position
        if (position == selectedItem) {
            v.setBackgroundColor(Color.rgb(255,152,0));
        }
        else {
            // for other views
            v.setBackgroundColor(Color.WHITE);

        }
        return setCentered(super.getDropDownView(position, v, parent));
    }
}

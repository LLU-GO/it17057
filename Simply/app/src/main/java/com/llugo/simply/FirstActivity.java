package com.llugo.simply;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.Collections;

public class FirstActivity extends AppCompatActivity {
    private int CAMERA_PERMISSION_CODE=1;
    private Button bStartMain;
    private Button bSkip;
    CodeScanner codeScanner;
    CodeScannerView codeScannerView;
    TextView tvResultData;
    String tvText="First you need to scan QR code!";
    String tvTextInfo="First you need to scan QR code!";

    static int[][][][]mazeMap;
    Spinner room_selection;
    ArrayList<String> roomFullInfoList;
    int totalMazeParts;
    int totalFloors;
    int totalRows;
    int totalCols;
    int[][][][] vertexId;
    ArrayList<Room> roomList;
    ArrayList<String> partNameList;
    ArrayList<Room> potentialMenWC;
    ArrayList<Room> potentialWomWC;
    MyCenteringArrayAdapter arrayAdapterRoomSelection;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("tvText", tvText);
        savedInstanceState.putString("tvTextInfo", tvTextInfo);
        savedInstanceState.putInt("selectedRoom", (int)room_selection.getSelectedItemId());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);
        tvText = savedInstanceState.getString("tvText");
        tvTextInfo = savedInstanceState.getString("tvTextInfo");
        if(tvText.matches("\\d+,\\d+,\\d+,\\d+")){
            tvResultData.setText(tvTextInfo);
        }
        else{
            tvResultData.setText(tvText);
        }
        arrayAdapterRoomSelection.selectedItem=savedInstanceState.getInt("selectedRoom");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        mazeMap=new int[8][4][][];
        EAST east=new EAST();
        mazeMap[0]=east.EAST;
        SE se = new SE();
        mazeMap[1]=se.SE;
        SOUTH south = new SOUTH();
        mazeMap[2]=south.SOUTH;
        SW sw=new SW();
        mazeMap[3]=sw.SW;
        WEST west= new WEST();
        mazeMap[4]=west.WEST;
        NW nw = new NW();
        mazeMap[5]=nw.NW;
        NORTH north=new NORTH();
        mazeMap[6]=north.NORTH;
        NE ne = new NE();
        mazeMap[7]=ne.NE;
        setContentView(R.layout.activity_first);
        room_selection = findViewById(R.id.room_selection2);
        roomFullInfoList = new ArrayList<String>();

        totalMazeParts=mazeMap.length;
        totalFloors=mazeMap[0].length;
        totalRows=mazeMap[0][0].length;
        totalCols=mazeMap[0][0][0].length;

        partNameList=new ArrayList<String>();
        if (totalMazeParts != 0) {
            partNameList.add("East");
            partNameList.add("SE");
            partNameList.add("South");
            partNameList.add("SW");
            partNameList.add("West");
            partNameList.add("NW");
            partNameList.add("North");
            partNameList.add("NE");
        }

        Do();

        if (roomList.size()!=0) {
            Collections.sort(roomList, (x, y) -> Integer.valueOf(Integer.parseInt(x.RoomNo)).compareTo(Integer.parseInt(y.RoomNo)));
            ArrayList<String> unnumberedRooms=new ArrayList<>();
            ArrayList<String> numberedRooms=new ArrayList<>();
            for (Room room : roomList)
            {
                switch (Integer.parseInt(room.RoomNo)){
                    case 0:
                    case 469:
                    case 621:
                    case 999:
                        unnumberedRooms.add(room.RoomFullInfo);
                        break;
                    default:
                        if(room.RoomFullInfo.contains("Closest")){
                            unnumberedRooms.add(room.RoomFullInfo);
                        }
                        else{
                            numberedRooms.add(room.RoomFullInfo);
                        }
                        break;
                }
            }
            Collections.sort(unnumberedRooms);
            roomFullInfoList.addAll(unnumberedRooms);
            roomFullInfoList.addAll(numberedRooms);
            arrayAdapterRoomSelection = new MyCenteringArrayAdapter(this, android.R.layout.simple_spinner_item,roomFullInfoList);
            room_selection.setAdapter(arrayAdapterRoomSelection);
        }

        codeScannerView=findViewById(R.id.scannerView);
        codeScanner= new CodeScanner(this,codeScannerView);
        tvResultData=findViewById(R.id.tvResultData);
        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvText=result.getText();
                        if(tvText.matches("\\d+,\\d+,\\d+,\\d+")){
                            String[] items = tvText.replaceAll("\\s", "").split(",");
                            switch (items[0]){
                                case "0":
                                    items[0]="EAST BLOCK";
                                    break;
                                case "1":
                                    items[0]="SOUTH EAST BLOCK";
                                    break;
                                case "2":
                                    items[0]="SOUTH BLOCK";
                                    break;
                                case "3":
                                    items[0]="SOUTH WEST BLOCK";
                                    break;
                                case "4":
                                    items[0]="WEST BLOCK";
                                    break;
                                case "5":
                                    items[0]="NORTH WEST BLOCK";
                                    break;
                                case "6":
                                    items[0]="NORTH BLOCK";
                                    break;
                                case "7":
                                    items[0]="NORTH EAST BLOCK";
                                    break;
                            }
                            switch (items[1]){
                                case "0":
                                    items[1]="FIRST FLOOR";
                                    break;
                                case "1":
                                    items[1]="SECOND FLOOR";
                                    break;
                                case "2":
                                    items[1]="THIRD FLOOR";
                                    break;
                                case "3":
                                    items[1]="FOURTH FLOOR";
                                    break;
                            }
                            tvTextInfo=items[0]+", "+items[1];
                            tvResultData.setText(tvTextInfo);
                        }
                        else{
                            tvResultData.setText(tvText);
                        }
                    }
                });
            }
        });

        codeScannerView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                requestForCamera();
            }
        });

        bStartMain=(Button)findViewById(R.id.bStartMain);
        bStartMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeScanner.stopPreview();
                openMainActivity();
            }
        });

        bSkip=(Button)findViewById(R.id.bSkip);
        bSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipMainActivity();
            }
        });

        room_selection.post(new Runnable() {
            public void run() {
                room_selection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        arrayAdapterRoomSelection.selectedItem = position;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();
        requestForCamera();
    }

    private void requestForCamera(){
        if(ContextCompat.checkSelfPermission(FirstActivity.this
        , Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED){
            Toast.makeText(FirstActivity.this,"Camera used on behalf of your agreement!", Toast.LENGTH_SHORT).show();
            codeScanner.startPreview();
        }
        else{
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)){
                new AlertDialog.Builder(this).setTitle("Camera permission needed").setMessage("This permission is needed to scan a QR code to determine your current location").setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(FirstActivity.this,new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                    }
                }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
            }
            else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==CAMERA_PERMISSION_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"Permission GRANTED", Toast.LENGTH_SHORT).show();
                codeScanner.startPreview();
            }
            else{
                Toast.makeText(this,"Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void skipMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        MainActivity.startLocation=new Quartet(-1,-1,-1,-1);
        MainActivity.destinationLocation= new Quartet(-1,-1,-1,-1);
        startActivity(intent);
    }

    public void openMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        String startLocationStringFA = tvText;
        String destinationLocationFA = room_selection.getSelectedItem().toString();
        if(startLocationStringFA.matches("\\d+,\\d+,\\d+,\\d+")){
            String[] items = startLocationStringFA.replaceAll("\\s", "").split(",");
            int[] results = new int[items.length];
            for (int i = 0; i < items.length; i++) {
                try {
                    results[i] = Integer.parseInt(items[i]);
                } catch (NumberFormatException nfe) {
                };
            }
            try{
                int test = mazeMap[results[0]][results[1]][results[2]][results[3]];
                if(test == 0){
                    notOurCodeAlert();
                    return;
                }
                intent.putExtra("startLocationFA",results);
            }
            catch (Exception ex){
                notOurCodeAlert();
                return;
            }
        }
        else{
            notOurCodeAlert();
            return;
        }
        if(room_selection.getSelectedItem().toString()!=""){
            intent.putExtra("destinationLocationFA",destinationLocationFA);
            intent.putExtra("fromFA",true);
        }
        startActivity(intent);
    }

    public void notOurCodeAlert(){
        android.app.AlertDialog.Builder dlgAlert  = new android.app.AlertDialog.Builder(this);
        dlgAlert.setMessage("This is not our QR code!");
        dlgAlert.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //dismiss the dialog
                    }
                });
        dlgAlert.create().show();
    }

    public void Do()
    {
        vertexId=new int[totalMazeParts][][][];
        int usableVertexCount = 0;
        roomList = new ArrayList<Room>();

        for (int mazePart=0; mazePart< totalMazeParts; mazePart++)
        {
            int totalFloors=mazeMap[mazePart].length;
            vertexId[mazePart]=new int[totalFloors][][];
            for (int floor = 0; floor < totalFloors; floor++)
            {
                int totalRows=mazeMap[mazePart][floor].length;
                vertexId[mazePart][floor]=new int[totalRows][];
                for (int row = 0; row < totalRows; row++)
                {
                    int totalCols=mazeMap[mazePart][floor][row].length;
                    vertexId[mazePart][floor][row]=new int[totalCols];
                    for (int col = 0; col < totalCols; col++)
                    {
                        if (mazeMap[mazePart][floor][row][col] != 0)
                        {
                            vertexId[mazePart][floor][row][col] = usableVertexCount;
                            usableVertexCount++;
                        }
                        else
                        {
                            vertexId[mazePart][floor][row][col] = -1;
                        }
                        if (mazeMap[mazePart][floor][row][col] > 3)
                        {
                            String roomInfo = "";
                            switch (mazeMap[mazePart][floor][row][col])
                            {
                                case 940:
                                case 9121:
                                case 9260:
                                case 944:
                                case 986:
                                    if((mazePart==6&&floor==0&&row==18&&col==10)||
                                            mazePart==0&&floor==0&&row==6&&col==4){
                                        roomInfo = "-2: MEN WC "+partNameList.get(mazePart)+" F"+floor;
                                    }
                                    else if((mazePart==6&&floor==0&&row==18&&col==12)||
                                            mazePart==0&&floor==0&&row==6&&col==8){
                                        roomInfo = "-3: WOM WC "+partNameList.get(mazePart)+" F"+floor;
                                    }
                                    else{
                                        roomInfo = ": MEN WC "+partNameList.get(mazePart)+" F"+floor;
                                    }
                                    break;
                                case 917:
                                case 9200:
                                case 9238:
                                case 9259:
                                    roomInfo = ": WOM WC "+partNameList.get(mazePart)+" F"+floor;
                                    break;
                                case 945:
                                    roomInfo = ": Cafeteria";
                                    break;
                                case 90:
                                    roomInfo = "Info Center";
                                    break;
                                case 9183:
                                    roomInfo = ": Jelgava Palace Museum";
                                    break;
                                case 9469:
                                    roomInfo = "Main Hall";
                                    break;
                                case 9621:
                                    roomInfo = "Silver Hall/Golden Hall";
                                    break;
                                case 919:
                                    roomInfo = ": Office of the Dean (LLU ITF)";
                                    break;
                                case 9234:
                                    roomInfo = ": Office of the Dean (LLU LF)";
                                    break;
                                case 935:
                                    roomInfo = "/36";
                                    break;
                                case 9230:
                                    roomInfo = "/231";
                                    break;
                                case 9145:
                                    if(mazePart==5&&floor==1&&row==9&&col==15){
                                        roomInfo = ": Room South Entrance";
                                    }
                                    else if(mazePart==5&&floor==1&&row==9&&col==21){
                                        roomInfo = ": Room North Entrance";
                                    }
                                    break;
                                case 9317:
                                    if(mazePart==4&&floor==3&&row==2&&col==31){
                                        roomInfo = ": Auditorium South Entrance";
                                    }
                                    else if(mazePart==4&&floor==3&&row==4&&col==38){
                                        roomInfo = ": Auditorium North Entrance";
                                    }
                                    break;
                                case 9318:
                                    if(mazePart==0&&floor==3&&row==5&&col==16){
                                        roomInfo = "-1";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==18){
                                        roomInfo = "-2";
                                    }
                                    break;
                                case 9322:
                                    if(mazePart==0&&floor==3&&row==6&&col==21){
                                        roomInfo = "-1";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==22){
                                        roomInfo = "-2";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==24){
                                        roomInfo = "-3";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==26){
                                        roomInfo = "-4";
                                    }
                                    else if(mazePart==0&&floor==3&&row==6&&col==27){
                                        roomInfo = "-5";
                                    }
                                    break;
                                case 9326:
                                    if(mazePart==0&&floor==3&&row==5&&col==30){
                                        roomInfo = "-1";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==32){
                                        roomInfo = "-2";
                                    }
                                    break;
                                case 923:
                                    if(mazePart==4&&floor==0&&row==5&&col==11){
                                        roomInfo = "-1";
                                    }
                                    break;
                                case 9286:
                                    if(mazePart==0&&floor==2&&row==7&&col==5){
                                        roomInfo = "-1";
                                    }
                                    break;
                                case 9999:
                                    if(mazePart==6&&floor==2&&row==17&&col==3){
                                        roomInfo = "Library West Entrance";
                                    }
                                    else if(mazePart==6&&floor==2&&row==39&&col==5){
                                        roomInfo = "Library East Entrance";
                                    }
                                    break;
                                case 9142:
                                    roomInfo = ": UNISEX WC "+partNameList.get(mazePart)+" F"+floor;
                                    break;
                                case 91:
                                    roomInfo = ": Students Self-Government (LLU SP)";
                                    break;
                            }
                            roomList.add(new Room(String.valueOf(mazeMap[mazePart][floor][row][col]).substring(1), roomInfo, mazePart, floor, row, col));
                        }
                    }
                }
            }
        }

        potentialMenWC = new ArrayList<Room>();
        potentialWomWC = new ArrayList<Room>();
        for (Room room:roomList) {
            if(room.RoomInfo.contains("MEN WC")) {
                potentialMenWC.add(room);
            }
            else if(room.RoomInfo.contains("WOM WC")){
                potentialWomWC.add(room);
            }
            else if(room.RoomInfo.contains("UNISEX WC")){
                potentialMenWC.add(room);
                potentialWomWC.add(room);
            }
        }
        if(potentialMenWC.size()>1){
            roomFullInfoList.add("Closest MEN WC");
        }
        if(potentialWomWC.size()>1){
            roomFullInfoList.add("Closest WOM WC");
        }
    }
}
package com.llugo.simply;

public class Room {
    public Room(String roomNo, String roomInfo, int mazePart, int floor, int row, int col)
    {
        RoomNo = roomNo;
        RoomInfo = roomInfo;
        switch(Integer.parseInt(roomNo)){
            case 0:
            case 469:
            case 621:
            case 999:
                RoomFullInfo = roomInfo;
                break;
            default:
                RoomFullInfo = String.format("%1s%2s", RoomNo, RoomInfo);
                break;
        }
        MazePart = mazePart;
        Floor = floor;
        Row = row;
        Col = col;
    }

    public String RoomNo;
    public String RoomInfo;
    public String RoomFullInfo;
    public int MazePart;
    public int Floor;
    public int Row;
    public int Col;
}

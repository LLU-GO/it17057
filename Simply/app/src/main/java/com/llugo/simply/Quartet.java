package com.llugo.simply;

public class Quartet
{
    public Quartet(int mazePart, int floor, int row, int col)
    {
        MazePart = mazePart;
        Floor = floor;
        Row = row;
        Col = col;
    }

    public int MazePart;
    public int Floor;
    public int Row;
    public int Col;
}

package com.llugo.simply;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    static int[][][][] mazeMap;
    static int totalMazeParts;
    static int totalFloors;
    static int totalRows;
    static int totalCols;
    static Quartet startLocation = new Quartet(-1,-1,-1,-1);
    static Quartet destinationLocation = new Quartet(-1,-1,-1,-1);
    static Stack<Quartet> Path = new Stack<Quartet>();
    List<Room> roomList;

    ArrayList<String> roomFullInfoList;
    PinchZoomImageView mImg;
    Spinner floor_selection;
    Spinner room_selection;
    Spinner buildingPart_selection;
    Button buttonGo;
    Button buttonUp;
    Button buttonDown;
    Button buttonCW;
    Button buttonCCW;
    MyCenteringArrayAdapter arrayAdapterFloorSelection;
    MyCenteringArrayAdapter arrayAdapterBuildingPartSelection;
    MyCenteringArrayAdapter arrayAdapterRoomSelection;
    Boolean varZimet2=false;
    ArrayList<String> partNameList;
    ArrayList<String> floorNumberList;
    ArrayList<Room> potentialMenWC;
    ArrayList<Room> potentialWomWC;
    boolean fromFA=false;
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("varZimet2", varZimet2);
        savedInstanceState.putBoolean("fromFA", fromFA);
        savedInstanceState.putInt("selectedFloor", (int)floor_selection.getSelectedItemId());
        savedInstanceState.putInt("selectedBuildingPart", (int)buildingPart_selection.getSelectedItemId());
        savedInstanceState.putInt("selectedRoom", (int)room_selection.getSelectedItemId());
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.

        varZimet2 = savedInstanceState.getBoolean("varZimet2");
        fromFA = savedInstanceState.getBoolean("fromFA");
        arrayAdapterRoomSelection.selectedItem=savedInstanceState.getInt("selectedRoom");
        arrayAdapterFloorSelection.selectedItem=savedInstanceState.getInt("selectedFloor");
        arrayAdapterBuildingPartSelection.selectedItem=savedInstanceState.getInt("selectedBuildingPart");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        if(extras != null){
            fromFA=extras.getBoolean("fromFA");
        }
        mazeMap=new int[8][4][][];
        EAST east=new EAST();
        mazeMap[0]=east.EAST;
        SE se = new SE();
        mazeMap[1]=se.SE;
        SOUTH south = new SOUTH();
        mazeMap[2]=south.SOUTH;
        SW sw=new SW();
        mazeMap[3]=sw.SW;
        WEST west= new WEST();
        mazeMap[4]=west.WEST;
        NW nw = new NW();
        mazeMap[5]=nw.NW;
        NORTH north=new NORTH();
        mazeMap[6]=north.NORTH;
        NE ne = new NE();
        mazeMap[7]=ne.NE;
        setContentView(R.layout.activity_main);
        room_selection = findViewById(R.id.room_selection);
        buildingPart_selection = findViewById(R.id.part_selection);
        floor_selection = findViewById(R.id.floor_selection);
        roomFullInfoList = new ArrayList<String>();
        floorNumberList=new ArrayList<String>();

        totalMazeParts=mazeMap.length;
        totalFloors=mazeMap[0].length;
        totalRows=mazeMap[0][0].length;
        totalCols=mazeMap[0][0][0].length;

        partNameList=new ArrayList<String>();
        if (totalMazeParts != 0) {
            partNameList.add("East");
            partNameList.add("SE");
            partNameList.add("South");
            partNameList.add("SW");
            partNameList.add("West");
            partNameList.add("NW");
            partNameList.add("North");
            partNameList.add("NE");
            arrayAdapterBuildingPartSelection = new MyCenteringArrayAdapter(this, android.R.layout.simple_spinner_item,partNameList);
            buildingPart_selection.setAdapter(arrayAdapterBuildingPartSelection);
        }

        Do();

        if (roomList.size()!=0) {
            Collections.sort(roomList, (x, y) -> Integer.valueOf(Integer.parseInt(x.RoomNo)).compareTo(Integer.parseInt(y.RoomNo)));
            ArrayList<String> unnumberedRooms=new ArrayList<>();
            ArrayList<String> numberedRooms=new ArrayList<>();
            for (Room room : roomList)
            {
                switch (Integer.parseInt(room.RoomNo)){
                    case 0:
                    case 469:
                    case 621:
                    case 999:
                        unnumberedRooms.add(room.RoomFullInfo);
                        break;
                    default:
                        if(room.RoomFullInfo.contains("Closest")){
                            unnumberedRooms.add(room.RoomFullInfo);
                        }
                        else{
                            numberedRooms.add(room.RoomFullInfo);
                        }
                        break;
                }
            }
            Collections.sort(unnumberedRooms);
            roomFullInfoList.addAll(unnumberedRooms);
            roomFullInfoList.addAll(numberedRooms);
            arrayAdapterRoomSelection = new MyCenteringArrayAdapter(this, android.R.layout.simple_spinner_item,roomFullInfoList);
            room_selection.setAdapter(arrayAdapterRoomSelection);
        }

        if (totalMazeParts != 0) {
            for (int f=0; f<totalFloors; f++)
            {
                floorNumberList.add(String.valueOf(f));
            }
            arrayAdapterFloorSelection = new MyCenteringArrayAdapter(this, android.R.layout.simple_spinner_item, floorNumberList);
            floor_selection.setAdapter(arrayAdapterFloorSelection);
        }

        buildingPart_selection.post(new Runnable() {
            public void run() {
                buildingPart_selection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        arrayAdapterBuildingPartSelection.selectedItem=position;
                        try{
                            mImg.setImageBitmap(bmList[(int)buildingPart_selection.getSelectedItemId()][Integer.parseInt(floor_selection.getSelectedItem().toString())]);

                        }
                        catch (NullPointerException e){}
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
        floor_selection.post(new Runnable() {
            public void run() {
                floor_selection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        arrayAdapterFloorSelection.selectedItem=position;
                        int totalFloors=mazeMap[(int)buildingPart_selection.getSelectedItemId()].length;
                        if(Integer.parseInt(floor_selection.getSelectedItem().toString()) <= totalFloors-1)
                        {
                            try{
                                mImg.setImageBitmap(bmList[(int)buildingPart_selection.getSelectedItemId()][Integer.parseInt(floor_selection.getSelectedItem().toString())]);

                            }
                            catch (NullPointerException e){}
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
        mImg = (PinchZoomImageView) findViewById(R.id.pinchZoomImageView);
        mImg.post(new Runnable() {
            @Override
            public void run() {
                if(fromFA){
                    Intent i = getIntent();
                    Bundle extras = i.getExtras();
                    if(extras!=null){
                        if(extras.containsKey("startLocationFA")){
                            int[] sLFA = extras.getIntArray("startLocationFA");
                            startLocation = new Quartet(sLFA[0],sLFA[1],sLFA[2],sLFA[3]);
                        }
                        if(extras.containsKey("destinationLocationFA")){
                            String dLFA = extras.getString("destinationLocationFA");
                            for (Room r: roomList
                            ) {
                                if(r.RoomFullInfo.equals(dLFA)&&!r.RoomFullInfo.contains("Closest")){
                                    destinationLocation = new Quartet(r.MazePart,r.Floor,r.Row,r.Col);
                                    buildingPart_selection.setSelection(r.MazePart);
                                    floor_selection.setSelection(r.Floor);
                                    int spinnerPosition = arrayAdapterRoomSelection.getPosition(dLFA);
                                    room_selection.setSelection(spinnerPosition);
                                }
                            }
                            if(dLFA.contains("Closest") && startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1){
                                int spinnerPosition = arrayAdapterRoomSelection.getPosition(dLFA);
                                room_selection.setSelection(spinnerPosition);
                            }
                        }
                    }
                }
                for (Room room: roomList)
                {
                    if(room.RoomFullInfo == room_selection.getSelectedItem()||room_selection.getSelectedItem().toString().contains("Closest")) {
                        if(room.RoomFullInfo == room_selection.getSelectedItem()&&!room_selection.getSelectedItem().toString().contains("Closest")){
                            destinationLocation.MazePart = room.MazePart;
                            destinationLocation.Floor = room.Floor;
                            destinationLocation.Row = room.Row;
                            destinationLocation.Col = room.Col;
                        }
                        try{
                            DrawMaze();
                            if(varZimet2||fromFA){
                                UseAlgorithmAndDraw();
                            }
                            if(startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1){
                                DrawStartLocation();
                            }
                            if(!fromFA){
                                mImg.setImageBitmap(bmList[(int)buildingPart_selection.getSelectedItemId()][Integer.parseInt(floor_selection.getSelectedItem().toString())]);
                                floor_selection.setSelection(Integer.parseInt(floor_selection.getSelectedItem().toString()));
                            }
                        }
                        catch (IllegalArgumentException e){}
                        if(room_selection.getSelectedItem().toString().contains("Closest")){
                            break;
                        }
                    }
                }
                if(fromFA){
                    varZimet2=true;
                    buildingPart_selection.setSelection(destinationLocation.MazePart);
                    floor_selection.setSelection(destinationLocation.Floor);
                    mImg.setImageBitmap(bmList[destinationLocation.MazePart][destinationLocation.Floor]);
                    fromFA=false;
                }
            }
        });
        room_selection.post(new Runnable() {
            public void run() {
                room_selection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        arrayAdapterRoomSelection.selectedItem = position;
                        for (Room room: roomList)
                        {
                            if(room.RoomFullInfo == room_selection.getSelectedItem() ||
                                    room_selection.getSelectedItem().toString().contains("Closest")) {
                                if(room.RoomFullInfo == room_selection.getSelectedItem() &&
                                        !room_selection.getSelectedItem().toString().contains("Closest")){
                                    destinationLocation.MazePart = room.MazePart;
                                    destinationLocation.Floor = room.Floor;
                                    destinationLocation.Row = room.Row;
                                    destinationLocation.Col = room.Col;
                                }
                                try{
                                    DrawMaze();
                                    if(startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1){
                                        varZimet2=true;
                                        UseAlgorithmAndDraw();
                                    }
                                    buildingPart_selection.setSelection(destinationLocation.MazePart);
                                    floor_selection.setSelection(destinationLocation.Floor);
                                    if(startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1){
                                        mImg.setImageBitmap(bmList[destinationLocation.MazePart][destinationLocation.Floor]);
                                    }
                                }
                                catch (IllegalArgumentException e){}
                                if(room_selection.getSelectedItem().toString().contains("Closest")){
                                    break;
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });

        findViewById(R.id.pinchZoomImageView).setOnTouchListener(new PinchZoomImageView.OnTouchListener()
        {
            GestureDetector gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public void onLongPress(MotionEvent e) {
                    int floor =Integer.parseInt(floor_selection.getSelectedItem().toString());
                    float rX=convertToCanvasXCoordinate(e.getX(),mImg.mCanvasClipBounds.left, mImg.mCanvasClipBounds.width(),mImg);
                    float rY=convertToCanvasYCoordinate(e.getY(),mImg.mCanvasClipBounds.top, mImg.mCanvasClipBounds.height(),mImg);
                    int mazePart=(int)buildingPart_selection.getSelectedItemId();
                    int totalRows=mazeMap[mazePart][floor].length;
                    for(int row = 0; row < totalRows; row++)
                    {
                        int totalCols=mazeMap[mazePart][floor][row].length;
                        for (int col = 0; col<totalCols; col++)
                        {
                            if(mazeMap[mazePart][floor][row][col] != 0 && (rX > cellList[mazePart][floor][row][col].left) &&
                                    (rX < cellList[mazePart][floor][row][col].left + cellList[mazePart][floor][row][col].right) &&
                                    (rY > cellList[mazePart][floor][row][col].top) &&
                                    (rY < cellList[mazePart][floor][row][col].top + cellList[mazePart][floor][row][col].bottom))
                            {
                                startLocation.MazePart = mazePart;
                                startLocation.Floor = floor;
                                startLocation.Row = row;
                                startLocation.Col = col;
                                DrawMaze();
                                DrawStartLocation();
                                varZimet2=false;
                            }
                        }
                    }
                }
            });
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        buttonGo=(Button)findViewById(R.id.buttonGO);
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startLocation.MazePart==destinationLocation.MazePart&&
                        startLocation.Floor == destinationLocation.Floor &&
                        startLocation.Row == destinationLocation.Row &&
                        startLocation.Col == destinationLocation.Col &&
                        (startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1))
                {
                    dlgAlert.setMessage("You are already at the room: " + room_selection.getSelectedItem());
                    dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //dismiss the dialog
                                }
                            });
                    dlgAlert.create().show();
                }
                else if (startLocation.MazePart==-1&&startLocation.Floor==-1&&startLocation.Row==-1&&startLocation.Col==-1)
                {
                    dlgAlert.setMessage("1st you have to pick your location!");
                    dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //dismiss the dialog
                                }
                            });
                    dlgAlert.create().show();
                }
                /*else if (isValid(startLocation.MazePart, startLocation.Floor, startLocation.Row, startLocation.Col) &&
                !isValid(destinationLocation.MazePart, destinationLocation.Floor, destinationLocation.Row, destinationLocation.Col))
                {
                    dlgAlert.setMessage("Now choose your destination!");
                    dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //dismiss the dialog
                                }
                            });
                    dlgAlert.create().show();
                }*/
                if(startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1){
                    varZimet2=true;
                    UseAlgorithmAndDraw();
                    mImg.setImageBitmap(bmList[startLocation.MazePart][startLocation.Floor]);
                    buildingPart_selection.setSelection(startLocation.MazePart);
                    floor_selection.setSelection(startLocation.Floor);
                }
            }
        });

        buttonUp=(Button)findViewById(R.id.buttonUP);
        buttonUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value=(int)floor_selection.getSelectedItemId();
                if(value<(int)floor_selection.getCount()-1){
                    value++;
                    floor_selection.setSelection(value);
                }
            }
        });

        buttonDown=(Button)findViewById(R.id.buttonDOWN);
        buttonDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value=(int)floor_selection.getSelectedItemId();
                if(value>0){
                    value--;
                    floor_selection.setSelection(value);
                }
            }
        });

        buttonCW=(Button)findViewById(R.id.buttonCW);
        buttonCW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value=(int)buildingPart_selection.getSelectedItemId();
                if(value<(int)buildingPart_selection.getCount()){
                    value++;
                }
                if(value==(int)buildingPart_selection.getCount()){
                    value=0;
                }
                buildingPart_selection.setSelection(value);
            }
        });

        buttonCCW=(Button)findViewById(R.id.buttonCCW);
        buttonCCW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value=(int)buildingPart_selection.getSelectedItemId();
                if(value>0){
                    value--;
                }
                else if(value==0){
                    value=(int)buildingPart_selection.getCount()-1;
                }
                buildingPart_selection.setSelection(value);
            }
        });

        if(!fromFA){
            buildingPart_selection.setSelection(0);
            arrayAdapterBuildingPartSelection.selectedItem=0;
            floor_selection.setSelection(1);
            arrayAdapterFloorSelection.selectedItem=1;
        }
    }

    public void Do()
    {
        roomList = new ArrayList<Room>();

        for (int mazePart=0; mazePart< totalMazeParts; mazePart++)
        {
            int totalFloors=mazeMap[mazePart].length;
            for (int floor = 0; floor < totalFloors; floor++)
            {
                int totalRows=mazeMap[mazePart][floor].length;
                for (int row = 0; row < totalRows; row++)
                {
                    int totalCols=mazeMap[mazePart][floor][row].length;
                    for (int col = 0; col < totalCols; col++)
                    {
                        if (mazeMap[mazePart][floor][row][col] > 3)
                        {
                            String roomInfo = "";
                            switch (mazeMap[mazePart][floor][row][col])
                            {
                                case 940:
                                case 9121:
                                case 9260:
                                case 944:
                                case 986:
                                    if((mazePart==6&&floor==0&&row==18&&col==10)||
                                            mazePart==0&&floor==0&&row==6&&col==4){
                                        roomInfo = "-2: MEN WC "+partNameList.get(mazePart)+" F"+floor;
                                    }
                                    else if((mazePart==6&&floor==0&&row==18&&col==12)||
                                            mazePart==0&&floor==0&&row==6&&col==8){
                                        roomInfo = "-3: WOM WC "+partNameList.get(mazePart)+" F"+floor;
                                    }
                                    else{
                                        roomInfo = ": MEN WC "+partNameList.get(mazePart)+" F"+floor;
                                    }
                                    break;
                                case 917:
                                case 9200:
                                case 9238:
                                case 9259:
                                    roomInfo = ": WOM WC "+partNameList.get(mazePart)+" F"+floor;
                                    break;
                                case 945:
                                    roomInfo = ": Cafeteria";
                                    break;
                                case 90:
                                    roomInfo = "Info Center";
                                    break;
                                case 9183:
                                    roomInfo = ": Jelgava Palace Museum";
                                    break;
                                case 9469:
                                    roomInfo = "Main Hall";
                                    break;
                                case 9621:
                                    roomInfo = "Silver Hall/Golden Hall";
                                    break;
                                case 919:
                                    roomInfo = ": Office of the Dean (LLU ITF)";
                                    break;
                                case 9234:
                                    roomInfo = ": Office of the Dean (LLU LF)";
                                    break;
                                case 935:
                                    roomInfo = "/36";
                                    break;
                                case 9230:
                                    roomInfo = "/231";
                                    break;
                                case 9145:
                                    if(mazePart==5&&floor==1&&row==9&&col==15){
                                        roomInfo = ": Room South Entrance";
                                    }
                                    else if(mazePart==5&&floor==1&&row==9&&col==21){
                                        roomInfo = ": Room North Entrance";
                                    }
                                    break;
                                case 9317:
                                    if(mazePart==4&&floor==3&&row==2&&col==31){
                                        roomInfo = ": Auditorium South Entrance";
                                    }
                                    else if(mazePart==4&&floor==3&&row==4&&col==38){
                                        roomInfo = ": Auditorium North Entrance";
                                    }
                                    break;
                                case 9318:
                                    if(mazePart==0&&floor==3&&row==5&&col==16){
                                        roomInfo = "-1";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==18){
                                        roomInfo = "-2";
                                    }
                                    break;
                                case 9322:
                                    if(mazePart==0&&floor==3&&row==6&&col==21){
                                        roomInfo = "-1";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==22){
                                        roomInfo = "-2";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==24){
                                        roomInfo = "-3";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==26){
                                        roomInfo = "-4";
                                    }
                                    else if(mazePart==0&&floor==3&&row==6&&col==27){
                                        roomInfo = "-5";
                                    }
                                    break;
                                case 9326:
                                    if(mazePart==0&&floor==3&&row==5&&col==30){
                                        roomInfo = "-1";
                                    }
                                    else if(mazePart==0&&floor==3&&row==5&&col==32){
                                        roomInfo = "-2";
                                    }
                                    break;
                                case 923:
                                    if(mazePart==4&&floor==0&&row==5&&col==11){
                                        roomInfo = "-1";
                                    }
                                    break;
                                case 9286:
                                    if(mazePart==0&&floor==2&&row==7&&col==5){
                                        roomInfo = "-1";
                                    }
                                    break;
                                case 9999:
                                    if(mazePart==6&&floor==2&&row==17&&col==3){
                                        roomInfo = "Library West Entrance";
                                    }
                                    else if(mazePart==6&&floor==2&&row==39&&col==5){
                                        roomInfo = "Library East Entrance";
                                    }
                                    break;
                                case 9142:
                                    roomInfo = ": UNISEX WC "+partNameList.get(mazePart)+" F"+floor;
                                    break;
                                case 91:
                                    roomInfo = ": Students Self-Government (LLU SP)";
                                    break;
                            }
                            roomList.add(new Room(String.valueOf(mazeMap[mazePart][floor][row][col]).substring(1), roomInfo, mazePart, floor, row, col));
                        }
                    }
                }
            }
        }

        potentialMenWC = new ArrayList<Room>();
        potentialWomWC = new ArrayList<Room>();
        for (Room room:roomList) {
            if(room.RoomInfo.contains("MEN WC")) {
                potentialMenWC.add(room);
            }
            else if(room.RoomInfo.contains("WOM WC")){
                potentialWomWC.add(room);
            }
            else if(room.RoomInfo.contains("UNISEX WC")){
                potentialMenWC.add(room);
                potentialWomWC.add(room);
            }
        }
        if(potentialMenWC.size()>1){
            roomFullInfoList.add("Closest MEN WC");
        }
        if(potentialWomWC.size()>1){
            roomFullInfoList.add("Closest WOM WC");
        }
    }

    private float convertToCanvasXCoordinate(float touchX,float offSetX,float viewportVisibleWidth, PinchZoomImageView pinchZoomImageV){
        return (touchX*viewportVisibleWidth)/pinchZoomImageV.getWidth()+offSetX;
    }

    private float convertToCanvasYCoordinate(float touchY,float offSetY,float viewportVisibleHeight, PinchZoomImageView pinchZoomImageV){
        return (touchY*viewportVisibleHeight)/pinchZoomImageV.getHeight()+offSetY;
    }

    static int[] rowNum90 = { -1, 0, 0, 1 };
    static int[] rowNum45 = { 1, -1, 1, -1 };
    static int[] colNum90 = { 0, -1, 1, 0 };
    static int[] colNum45 = { 1, -1, -1, 1 };

    static boolean isValid(int mazePart, int floor, int row, int col)
    {
        int totalFloors=mazeMap[mazePart].length-1;
        int totalRows=mazeMap[mazePart][totalFloors].length-1;
        int totalCols=mazeMap[mazePart][totalFloors][totalRows].length-1;
        return (row >= 0) && (row <= totalRows) &&
                (col >= 0) && (col <= totalCols) &&
                (floor >= 0) && (floor <= totalFloors) &&
                (mazePart >= 0) && (mazePart <= totalMazeParts-1);
    }

    public static Stack<Quartet> findPathBFS(Quartet start, Quartet end, int[][][][] maze)
    {
        // Get the x and y values from both start and end Pos
        // currX and currY is initially start Pos
        int currT = start.MazePart;
        int currZ = start.Floor;
        int currX = start.Row;
        int currY = start.Col;
        int endT = end.MazePart;
        int endZ = end.Floor;
        int endX = end.Row;
        int endY = end.Col;
        boolean[][][][] visited = new boolean[totalMazeParts][][][];
        // Set to size of maze
        for (int mazePart=0; mazePart< totalMazeParts; mazePart++)
        {
            int totalFloors=maze[mazePart].length;
            visited[mazePart]=new boolean[totalFloors][][];
            for (int floor = 0; floor < totalFloors; floor++) {
                int totalRows = maze[mazePart][floor].length;
                visited[mazePart][floor] = new boolean[totalRows][];
                for (int row = 0; row < totalRows; row++) {
                    int totalCols = maze[mazePart][floor][row].length;
                    visited[mazePart][floor][row] = new boolean[totalCols];
                }
            }
        }

        Queue<Node> q = new LinkedList<Node>();

        visited[currT][currZ][currX][currY] = true;
        q.add(new Node(currT, currZ, currX, currY,null));

        while (q.size() != 0)
        {
            Node node = q.remove();

            currT = node.t;
            currZ = node.z;
            currX = node.x;
            currY = node.y;

            if (currT == endT && currZ == endZ && currX == endX && currY == endY)
            {
                Stack<Quartet> path = new Stack<Quartet>();
                do
                {
                    path.push(new Quartet(node.t, node.z, node.x, node.y));
                    node = node.prevNode;
                } while (node != null);
                return path;
            }

            for (int k = 0; k < 4; k++)
            {
                try{
                    if (isValidBFS(visited, currT, currZ, currX + rowNum90[k], currY + colNum90[k]))
                    {
                        visited[currT][currZ][currX + rowNum90[k]][currY + colNum90[k]] = true;
                        q.add(new Node(currT, currZ, currX + rowNum90[k], currY + colNum90[k], node));
                    }
                }
                catch (ArrayIndexOutOfBoundsException ex){}
            }
            for (int k = 0; k < 4; k++)
            {
                try{
                    if (isValidBFS(visited, currT, currZ, currX + rowNum45[k], currY + colNum45[k]) &&
                            (mazeMap[currT][currZ][currX + rowNum45[k]][currY] != 0 || mazeMap[currT][currZ][currX][currY + colNum45[k]] != 0))
                    {
                        visited[currT][currZ][currX + rowNum45[k]][currY + colNum45[k]] = true;
                        q.add(new Node(currT, currZ, currX + rowNum45[k], currY + colNum45[k], node));
                    }
                }
                catch (ArrayIndexOutOfBoundsException ex){}
            }
            if (currZ<totalFloors-1 && mazeMap[currT][currZ + 1][currX][currY] == 2 && mazeMap[currT][currZ][currX][currY] == 2)
            {
                visited[currT][currZ + 1][currX][currY] = true;
                q.add(new Node(currT,currZ + 1, currX, currY, node));
            }
            else if (currZ>0&& mazeMap[currT][currZ - 1][currX][currY] == 2 && mazeMap[currT][currZ][currX][currY] == 2)
            {
                visited[currT][currZ - 1][currX][currY] = true;
                q.add(new Node(currT,currZ - 1, currX, currY, node));
            }
            //clockwise
            //rl
            if (currT<totalMazeParts-1 && currY==0&&mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalCols=mazeMap[currT+1][currZ][currX].length-1;
                int totalRows=mazeMap[currT+1][currZ].length-1;
                for(int row=0;row<totalRows;row++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT+1][currZ][row][totalCols]){
                        visited[currT+1][currZ][row][totalCols] = true;
                        q.add(new Node(currT+1,currZ, row, totalCols, node));
                    }
                }
            }
            //lr
            if (currT>0&&currT<totalMazeParts-1&&currY==mazeMap[currT][currZ][currX].length-1 && mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalRows=mazeMap[currT+1][currZ].length-1;
                for(int row=0;row<totalRows;row++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT+1][currZ][row][0]){
                        visited[currT+1][currZ][row][0] = true;
                        q.add(new Node(currT+1,currZ, row, 0 , node));
                    }
                }
            }
            //du
            if (currT<totalMazeParts-1 && currX==0&&mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalCols=mazeMap[currT+1][currZ][currX].length-1;
                int totalRows=mazeMap[currT+1][currZ].length-1;
                for(int col=0;col<totalCols;col++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT+1][currZ][totalRows][col]){
                        visited[currT+1][currZ][totalRows][col] = true;
                        q.add(new Node(currT+1,currZ, totalRows, col , node));
                    }
                }
            }
            //ud
            if (currT<totalMazeParts-1 && currX==mazeMap[currT][currZ].length-1&&mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalCols=mazeMap[currT+1][currZ][0].length-1;
                for(int col=0;col<totalCols;col++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT+1][currZ][0][col]){
                        visited[currT+1][currZ][0][col] = true;
                        q.add(new Node(currT+1,currZ, 0, col , node));
                    }
                }
            }
            //last-first
            if(currT==totalMazeParts-1&&currY==0&& mazeMap[currT][currZ][currX][currY]==3){
                int totalCols=mazeMap[0][currZ][currX].length-1;
                int totalRows=mazeMap[currT][currZ].length-1;
                for(int row=0;row<totalRows;row++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[0][currZ][row][totalCols]){
                        visited[0][currZ][row][totalCols] = true;
                        q.add(new Node(0,currZ, row, totalCols , node));
                    }
                }
            }
            //reverse clockwise
            //lr
            if (currT>0 && currY==mazeMap[currT][currZ][currX].length-1&&mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalRows=mazeMap[currT-1][currZ].length-1;
                for(int row=0;row<totalRows;row++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT-1][currZ][row][0]){
                        visited[currT-1][currZ][row][0] = true;
                        q.add(new Node(currT-1,currZ, row, 0, node));
                    }
                }
            }
            //rl
            if (currT>0&&currT<totalMazeParts-1&&currY==0 && mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalCols=mazeMap[currT-1][currZ][currX].length-1;
                int totalRows=mazeMap[currT-1][currZ].length-1;
                for(int row=0;row<totalRows;row++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT-1][currZ][row][totalCols]){
                        visited[currT-1][currZ][row][totalCols] = true;
                        q.add(new Node(currT-1,currZ, row, totalCols , node));
                    }
                }
            }
            //ud
            if (currT>0 && currX==0&&mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalCols=mazeMap[currT-1][currZ][currX].length-1;
                int totalRows=mazeMap[currT-1][currZ].length-1;
                for(int col=0;col<totalCols;col++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT-1][currZ][totalRows][col]){
                        visited[currT-1][currZ][totalRows][col] = true;
                        q.add(new Node(currT-1,currZ, totalRows, col , node));
                    }
                }
            }
            //du
            if (currT>0 && currX==mazeMap[currT][currZ].length-1&&mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalCols=mazeMap[currT-1][currZ][0].length-1;
                for(int col=0;col<totalCols;col++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[currT-1][currZ][0][col]){
                        visited[currT-1][currZ][0][col] = true;
                        q.add(new Node(currT-1,currZ, 0, col , node));
                    }
                }
            }
            //first-last
            if(currT==0&&currY==mazeMap[currT][currZ][currX].length-1&& mazeMap[currT][currZ][currX][currY]==3)
            {
                int totalRows=mazeMap[totalMazeParts-1][currZ].length-1;
                for(int row=0;row<totalRows;row++){
                    if(mazeMap[currT][currZ][currX][currY]==mazeMap[totalMazeParts-1][currZ][row][0]){
                        visited[totalMazeParts-1][currZ][row][0] = true;
                        q.add(new Node(totalMazeParts-1,currZ, row, 0 , node));
                    }
                }
            }

        }
        return null;
    }

    static boolean isValidBFS(boolean[][][][] visited, int t, int f, int r, int c)
    {
        return isValid(t, f, r, c) && !visited[t][f][r][c] && (mazeMap[t][f][r][c] != 0);
    }

    List<PointF> pathPoints;
    RectF[][][][] cellList;
    static Bitmap[][] bmList;
    static Canvas[][] cvList;
    Paint myPaint = new Paint();
    void DrawMaze()
    {
        float cellWid=0;
        float cellHgt=0;
        float yMin=0;
        float xMin=0;
        bmList=new Bitmap[totalMazeParts][];
        cvList=new Canvas[totalMazeParts][];
        cellList = new RectF[totalMazeParts][][][];
        for(int mazePart=0;mazePart<totalMazeParts;mazePart++)
        {
            pathPoints = new ArrayList<PointF>();
            cellList[mazePart]=new RectF[totalFloors][][];
            int totalFloors=mazeMap[mazePart].length;
            bmList[mazePart] = new Bitmap[totalFloors];
            cvList[mazePart] = new Canvas[totalFloors];
            for (int floor = 0; floor < totalFloors; floor++)
            {
                int totalRows=mazeMap[mazePart][floor].length;
                cellList[mazePart][floor]=new RectF[totalRows][];
                Bitmap.Config c = Bitmap.Config.RGB_565;
                bmList[mazePart][floor] = Bitmap.createBitmap(mImg.getWidth(),mImg.getHeight(), c);
                cvList[mazePart][floor] = new Canvas(bmList[mazePart][floor]);
                cvList[mazePart][floor].drawColor(Color.WHITE);
                for (int row = 0; row < totalRows; row++)
                {
                    int totalCols=mazeMap[mazePart][floor][row].length;
                    cellList[mazePart][floor][row]=new RectF[totalCols];
                    cellWid = mImg.getWidth() / totalCols;
                    cellHgt = mImg.getHeight() / totalRows;
                    if (cellWid > cellHgt)
                    {
                        cellWid = cellHgt;
                    }
                    else
                    {
                        cellHgt = cellWid;
                    }
                    xMin = (mImg.getWidth() - totalCols * cellWid) / 2;
                    yMin = (mImg.getHeight() - totalRows * cellHgt) / 2;
                    float yCoord = yMin + cellHgt * row;

                    for (int col = 0; col < totalCols; col++)
                    {
                        float xCoord = xMin + cellWid * col;
                        RectF cellBounds = new RectF(xCoord, yCoord, cellWid, cellHgt);
                        cellList[mazePart][floor][row][col] = cellBounds;

                        if (mazeMap[mazePart][floor][row][col] == 0)
                        {
                            myPaint.setColor(Color.rgb(255,152,0));
                            cvList[mazePart][floor].drawRect(xCoord,yCoord,xCoord+cellWid,yCoord+cellHgt, myPaint);
                            myPaint.setColor(Color.WHITE);
                            myPaint.setStyle(Paint.Style.STROKE);
                            myPaint.setColor(Color.rgb(251,182,59));
                            cvList[mazePart][floor].drawRect(xCoord,yCoord,xCoord+cellWid,yCoord+cellHgt, myPaint);
                            myPaint.setColor(Color.rgb(255,152,0));
                            myPaint.setStyle(Paint.Style.FILL);
                        }
                        if (mazeMap[mazePart][floor][row][col] == 2)
                        {
                            myPaint.setAntiAlias(true);
                            myPaint.setColor(Color.GRAY);
                            cvList[mazePart][floor].drawCircle(xCoord + (cellWid/2),yCoord+(cellHgt/2),(cellWid/2) - 2, myPaint);
                            myPaint.setAntiAlias(false);
                        }
                        if (mazeMap[mazePart][floor][row][col] == 3)
                        {
                            myPaint.setAntiAlias(true);
                            myPaint.setColor(Color.rgb(247,127,235));
                            cvList[mazePart][floor].drawCircle(xCoord + (cellWid/2),yCoord+(cellHgt/2),(cellWid/2) - 2, myPaint);
                            myPaint.setAntiAlias(false);
                        }

                        if (destinationLocation.Row != -1 && mazePart == destinationLocation.MazePart && floor == destinationLocation.Floor &&
                                row == destinationLocation.Row && col == destinationLocation.Col)
                        {
                            myPaint.setAntiAlias(true);
                            myPaint.setColor(Color.RED);
                            cvList[mazePart][floor].drawCircle(xCoord + (cellWid/2),yCoord+(cellHgt/2),(cellWid/2) - 2, myPaint);
                            myPaint.setAntiAlias(false);
                        }
                        else if (startLocation.Row != -1 && mazePart == startLocation.MazePart && floor == startLocation.Floor &&
                                row == startLocation.Row && col == startLocation.Col)
                        {
                            myPaint.setAntiAlias(true);
                            myPaint.setColor(Color.rgb(0,128,0));
                            cvList[mazePart][floor].drawCircle(xCoord + (cellWid/2),yCoord+(cellHgt/2),(cellWid/2) - 2, myPaint);
                            myPaint.setAntiAlias(false);
                        }
                    }
                }
                if (roomList.size() != 0)
                {
                    for (Room room : roomList) {
                        float yCoord = yMin + cellHgt * room.Row;
                        float xCoord = xMin + cellWid * room.Col;
                        if(mazePart==room.MazePart&&floor==room.Floor){
                            if (mazeMap[room.MazePart][room.Floor][room.Row][room.Col+1] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row][room.Col-1] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row-1][room.Col] == 0)
                            {
                                yCoord = yMin + cellHgt * (room.Row-1);
                            }
                            else if (mazeMap[room.MazePart][room.Floor][room.Row][room.Col + 1] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row][room.Col - 1] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row + 1][room.Col] == 0)
                            {
                                yCoord = yMin + cellHgt * (room.Row + 1);
                            }
                            else if (mazeMap[room.MazePart][room.Floor][room.Row][room.Col + 1] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row - 1][room.Col] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row + 1][room.Col] == 0)
                            {
                                xCoord = xMin + cellWid * (room.Col + 1);
                            }
                            else if (mazeMap[room.MazePart][room.Floor][room.Row][room.Col - 1] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row - 1][room.Col] == 0 &&
                                    mazeMap[room.MazePart][room.Floor][room.Row + 1][room.Col] == 0)
                            {
                                xCoord = xMin + cellWid * (room.Col - 1);
                            }

                            int foreColor;
                            int backColor;
                            String textToDraw="";
                            if (room.RoomInfo.equals(": MEN WC "+partNameList.get(mazePart)+" F"+room.Floor)||
                                    room.RoomInfo.equals("-2: MEN WC "+partNameList.get(mazePart)+" F"+room.Floor))
                            {
                                textToDraw = "WC";
                                foreColor = Color.rgb(0,0,255);
                                backColor = Color.rgb(173,216,230);
                            }
                            else if(room.RoomInfo.equals(": WOM WC "+partNameList.get(mazePart)+" F"+room.Floor)||
                                    room.RoomInfo.equals("-3: WOM WC "+partNameList.get(mazePart)+" F"+room.Floor))
                            {
                                textToDraw = "WC";
                                foreColor = Color.rgb(255,0,0);
                                backColor = Color.rgb(255,182,193);
                            }
                            else if(room.RoomInfo.equals(": UNISEX WC "+partNameList.get(mazePart)+" F"+room.Floor))
                            {
                                textToDraw = "WC";
                                foreColor = Color.rgb(0,128,0);
                                backColor = Color.WHITE;
                            }
                            else if(room.RoomInfo.equals("Info Center"))
                            {
                                textToDraw = "info";
                                foreColor = Color.rgb(255, 255,255);
                                backColor = Color.rgb(0,0,255);
                            }
                            else if(room.RoomInfo.equals("Main Hall"))
                            {
                                textToDraw = "hall";
                                foreColor = Color.rgb(255, 255,255);
                                backColor = Color.rgb(0,0,0);
                            }
                            else if(room.RoomInfo.equals("Silver Hall/Golden Hall"))
                            {
                                textToDraw = "hall";
                                foreColor = Color.rgb(0, 0,0);
                                backColor = Color.rgb(192,192,192);
                            }
                            else if(room.RoomInfo.equals("Library East Entrance")||room.RoomInfo.equals("Library West Entrance"))
                            {
                                textToDraw = "lib";
                                foreColor = Color.rgb(255, 255,255);
                                backColor = Color.rgb(102,0,51);
                            }
                            else if(room.RoomInfo.equals(": Cafeteria"))
                            {
                                textToDraw = "caf";
                                foreColor = Color.rgb(0,128,0);
                                backColor = Color.WHITE;
                            }
                            else if(room.RoomInfo.equals(": Office of the Dean (LLU ITF)"))
                            {
                                textToDraw = "ITF";
                                foreColor = Color.rgb(0,128,0);
                                backColor = Color.WHITE;
                            }
                            else if(room.RoomInfo.equals(": Office of the Dean (LLU LF)"))
                            {
                                textToDraw = "LF";
                                foreColor = Color.rgb(0,128,0);
                                backColor = Color.WHITE;
                            }
                            else if(room.RoomInfo.equals(": Jelgava Palace Museum"))
                            {
                                textToDraw = "mus";
                                foreColor = Color.rgb(255,255,255);
                                backColor = Color.rgb(66,32,98);
                            }
                            else if(room.RoomInfo.equals(": Students Self-Government (LLU SP)"))
                            {
                                textToDraw = "SP";
                                foreColor = Color.rgb(0,128,0);
                                backColor = Color.WHITE;
                            }
                            else
                            {
                                textToDraw = room.RoomNo;
                                foreColor = Color.rgb(0,128,0);
                                backColor = Color.WHITE;
                            }

                            myPaint.setColor(backColor);
                            cvList[room.MazePart][room.Floor].drawRect(xCoord,yCoord,xCoord+cellWid,yCoord+cellHgt, myPaint);
                            myPaint.setStyle(Paint.Style.STROKE);
                            myPaint.setColor(Color.BLACK);
                            cvList[room.MazePart][room.Floor].drawRect(xCoord,yCoord,xCoord+cellWid,yCoord+cellHgt, myPaint);
                            myPaint.setStyle(Paint.Style.FILL);

                            myPaint.setColor(foreColor);
                            myPaint.setTextAlign(Paint.Align.CENTER);
                            myPaint.setAntiAlias(true);
                            myPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                            myPaint.setTextSize(cellHgt/2);
                            cvList[room.MazePart][room.Floor].drawText(textToDraw,xCoord + (cellWid / 2) - (1/2),yCoord + (cellHgt / 2) - ((myPaint.descent() + myPaint.ascent()) / 2) + 1,myPaint);
                            myPaint.setAntiAlias(false);
                            myPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
                        }
                    }
                }
            }
        }

        for (Quartet quartet : Path) {
            int totalRows=mazeMap[quartet.MazePart][quartet.Floor].length;
            int totalCols=mazeMap[quartet.MazePart][quartet.Floor][quartet.Row].length;
            cellWid = mImg.getWidth() / totalCols;
            cellHgt = mImg.getHeight() / totalRows;
            if (cellWid > cellHgt)
            {
                cellWid = cellHgt;
            }
            else
            {
                cellHgt = cellWid;
            }
            xMin = (mImg.getWidth() - totalCols * cellWid) / 2;
            yMin = (mImg.getHeight() - totalRows * cellHgt) / 2;
            float yCoord = yMin + cellHgt * quartet.Row;
            float cy = yCoord + cellHgt / 2;
            float xCoord = xMin + cellWid * quartet.Col;
            float cx = xCoord + cellWid / 2;
            PointF currentPoint = new PointF(cx, cy);
            pathPoints.add(currentPoint);
        }
        Collections.reverse(pathPoints);

        mImg.setImageBitmap(bmList[(int)buildingPart_selection.getSelectedItemId()][Integer.parseInt(floor_selection.getSelectedItem().toString())]);
    }

    void DrawStartLocation() {
        RectF currRect = cellList[startLocation.MazePart][startLocation.Floor][startLocation.Row][startLocation.Col];
        myPaint.setAntiAlias(true);
        myPaint.setColor(Color.rgb(0,128,0));
        cvList[startLocation.MazePart][startLocation.Floor].drawCircle(currRect.left + (currRect.right / 2), currRect.top + (currRect.bottom / 2), (currRect.right / 2) - 2, myPaint);
        myPaint.setAntiAlias(false);
        mImg.setImageBitmap(bmList[startLocation.MazePart][startLocation.Floor]);
    }

    Paint mPathPaint = new Paint();
    float[] intervals = new float[]{7, 4};
    void DrawPathArrows()
    {
        List<Quartet> pathList = new ArrayList<Quartet>(Path);
        Collections.reverse(pathList);

        DashPathEffect dashPathEffect = new DashPathEffect(intervals,0);
        mPathPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        mPathPaint.setPathEffect(dashPathEffect);
        mPathPaint.setColor(Color.rgb(0,0,128));
        mPathPaint.setStrokeWidth(2);
        mPathPaint.setAntiAlias(true);
        for (int partId = 0; partId < pathPoints.size(); partId++) {
            if (partId == 0 || (partId > 0 && mazeMap[pathList.get(partId - 1).MazePart][pathList.get(partId - 1).Floor][pathList.get(partId - 1).Row][pathList.get(partId - 1).Col] != 2 ||
                    partId < pathPoints.size() - 1 && mazeMap[pathList.get(partId).MazePart][pathList.get(partId).Floor][pathList.get(partId).Row][pathList.get(partId).Col] != 2))
            {
                if (partId>0&&pathList.get(partId).MazePart == destinationLocation.MazePart && pathList.get(partId).Floor == destinationLocation.Floor &&
                        pathList.get(partId).Row == destinationLocation.Row && pathList.get(partId).Col == destinationLocation.Col)
                {
                    cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor].drawLine(pathPoints.get(partId - 1).x, pathPoints.get(partId - 1).y, pathPoints.get(partId).x, pathPoints.get(partId).y, mPathPaint);
                    drawArrow(mPathPaint,cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor],pathPoints.get(partId - 1).x, pathPoints.get(partId - 1).y, pathPoints.get(partId).x, pathPoints.get(partId).y);
                    return;
                }

                if (mazeMap[pathList.get(partId).MazePart][pathList.get(partId).Floor][pathList.get(partId).Row][pathList.get(partId).Col]!=3&&partId != 0 && partId < pathPoints.size() - 1 &&
                        pathList.get(partId).MazePart==pathList.get(partId+1).MazePart&&pathList.get(partId).MazePart==pathList.get(partId-1).MazePart&&
                        ((pathList.get(partId - 1).Row == pathList.get(partId).Row && pathList.get(partId + 1).Row != pathList.get(partId).Row) ||
                                (pathList.get(partId - 1).Row != pathList.get(partId).Row && pathList.get(partId + 1).Row == pathList.get(partId).Row) ||
                                (pathList.get(partId - 1).Col == pathList.get(partId).Col && pathList.get(partId + 1).Col != pathList.get(partId).Col) ||
                                (pathList.get(partId - 1).Col != pathList.get(partId).Col && pathList.get(partId + 1).Col == pathList.get(partId).Col)))
                {
                    drawArrow(mPathPaint,cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor],pathPoints.get(partId - 1).x, pathPoints.get(partId - 1).y, pathPoints.get(partId).x, pathPoints.get(partId).y);

                }
                if (partId != 0 && partId < pathPoints.size() - 1 && pathList.get(partId).MazePart==pathList.get(partId+1).MazePart&&pathList.get(partId).MazePart==pathList.get(partId-1).MazePart&&
                        ((pathList.get(partId - 1).Col == pathList.get(partId + 1).Col && pathList.get(partId).Col != pathList.get(partId - 1).Col && pathList.get(partId).Col != pathList.get(partId + 1).Col) ||
                                (pathList.get(partId - 1).Row == pathList.get(partId + 1).Row && pathList.get(partId).Row != pathList.get(partId - 1).Row && pathList.get(partId).Row != pathList.get(partId + 1).Row)))
                {
                    drawArrow(mPathPaint,cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor],pathPoints.get(partId - 1).x, pathPoints.get(partId - 1).y, pathPoints.get(partId).x, pathPoints.get(partId).y);
                }
                else if(partId != 0 && partId < pathPoints.size() - 1 && ((pathList.get(partId).Col==0&&
                        pathList.get(partId+1).Col==mazeMap[pathList.get(partId+1).MazePart][pathList.get(partId+1).Floor][pathList.get(partId+1).Row].length-1) ||
                        (pathList.get(partId).Col==mazeMap[pathList.get(partId).MazePart][pathList.get(partId).Floor][pathList.get(partId).Row].length-1&&pathList.get(partId+1).Col==0) ||
                        (pathList.get(partId).Row==0&&pathList.get(partId+1).Row==mazeMap[pathList.get(partId+1).MazePart][pathList.get(partId+1).Floor].length-1) ||
                        (pathList.get(partId).Row==mazeMap[pathList.get(partId).MazePart][pathList.get(partId).Floor].length-1&&pathList.get(partId+1).Row==0)))
                {
                    drawArrow(mPathPaint,cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor],pathPoints.get(partId - 1).x, pathPoints.get(partId - 1).y, pathPoints.get(partId).x, pathPoints.get(partId).y);
                }
                else if (partId == 0&& partId<pathPoints.size()-1&&mazeMap[pathList.get(partId).MazePart][pathList.get(partId).Floor][pathList.get(partId).Row][pathList.get(partId).Col]!=3)
                {
                    cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor].drawLine(pathPoints.get(partId).x, pathPoints.get(partId).y, pathPoints.get(partId + 1).x, pathPoints.get(partId + 1).y, mPathPaint);
                }
                if (partId != 0 && pathList.get(partId).MazePart==pathList.get(partId-1).MazePart && pathList.get(partId).Floor==pathList.get(partId-1).Floor)
                {
                    cvList[pathList.get(partId).MazePart][pathList.get(partId).Floor].drawLine(pathPoints.get(partId - 1).x, pathPoints.get(partId - 1).y, pathPoints.get(partId).x, pathPoints.get(partId).y, mPathPaint);
                }
            }
            // mImg.setImageBitmap(bmList[Integer.valueOf(floor_selection.getSelectedItem().toString())]);
        }
    }
    private void drawArrow(Paint paint, Canvas canvas, float from_x, float from_y, float to_x, float to_y)
    {
        float angle,anglerad, radius, lineangle;

        radius=15;
        angle=45;

        anglerad= (float) (Math.PI*angle/180.0f);
        lineangle= (float) (Math.atan2(to_y-from_y,to_x-from_x));

        canvas.drawLine(from_x,from_y,to_x,to_y,paint);

        Path path = new Path();
        path.setFillType(android.graphics.Path.FillType.EVEN_ODD);
        path.moveTo(to_x, to_y);
        path.lineTo((float)(to_x-radius*Math.cos(lineangle - (anglerad / 2.0))),
                (float)(to_y-radius*Math.sin(lineangle - (anglerad / 2.0))));
        path.lineTo((float)(to_x-radius*Math.cos(lineangle + (anglerad / 2.0))),
                (float)(to_y-radius*Math.sin(lineangle + (anglerad / 2.0))));
        path.close();

        canvas.drawPath(path, paint);
    }
    void UseAlgorithmAndDraw()
    {
        if(startLocation.MazePart!=-1&&startLocation.Floor!=-1&&startLocation.Row!=-1&&startLocation.Col!=-1 && startLocation!=destinationLocation){
            if(room_selection.getSelectedItem()=="Closest MEN WC"||room_selection.getSelectedItem()=="Closest WOM WC"){
                Map<Room,Stack<Quartet>> pathResults=new HashMap<Room,Stack<Quartet>>();
                ArrayList<Thread>tl=new ArrayList<Thread>();
                for (Room room : (room_selection.getSelectedItem() == "Closest MEN WC" ? potentialMenWC : (room_selection.getSelectedItem() == "Closest WOM WC" ? potentialWomWC : null))) {
                    Thread tr=new Thread(()->{
                        Stack<Quartet>path=new Stack<Quartet>();
                        pathResults.put(room,path = findPathBFS(startLocation, new Quartet(room.MazePart,room.Floor,room.Row,room.Col), mazeMap));
                    });
                    tl.add(tr);
                }
                for (Thread t:tl
                ) {
                    t.start();
                }
                for (Thread t:tl
                ) {
                    try{
                        t.join();
                    }
                    catch (InterruptedException e){}
                }
                Integer minValue=pathResults.entrySet().iterator().next().getValue().size();
                Map.Entry<Room,Stack<Quartet>> result=pathResults.entrySet().iterator().next();
                for (Map.Entry<Room,Stack<Quartet>> r:pathResults.entrySet()){
                    if(r.getValue().size()<minValue){
                        minValue=r.getValue().size();
                        result=r;
                    }
                }
                Path= result.getValue();
                destinationLocation=new Quartet(result.getKey().MazePart,result.getKey().Floor,result.getKey().Row,result.getKey().Col);
                mImg.setImageBitmap(bmList[destinationLocation.MazePart][destinationLocation.Floor]);
            }
            else{
                Path = findPathBFS(startLocation, destinationLocation, mazeMap);
            }
            DrawMaze();
            DrawPathArrows();
            DrawStartLocation();
        }
    }
}
package com.llugo.simply;

public class Node
{
    public int t;
    public int z;
    public int x;
    public int y;
    public Node prevNode;

    public Node(int t, int z, int x, int y, Node prevNode)
    {
        this.t = t;
        this.z = z;
        this.x = x;
        this.y = y;
        this.prevNode = prevNode;
    }
};
